import 'package:flutter/material.dart';
import 'package:squadre/utils/Utils.dart';
import 'package:squadre/widgets/video_player/aspect_ratio_video.dart';
import 'package:squadre/widgets/video_player/network_player_lifecycle.dart';
import 'package:video_player/video_player.dart';

class VideoPreview extends StatelessWidget {
  final String path;

  const VideoPreview({Key key, this.path}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Video Preview'),
        backgroundColor: Utils.pinkColor,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          color: Utils.colorWhite,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Center(
        child: NetworkPlayerLifeCycle(
          path,
          (BuildContext context, VideoPlayerController controller) =>
              OBAspectRatioVideo(controller),
        ),
      ),
    );
  }
}
