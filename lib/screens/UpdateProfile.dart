import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:squadre/utils/Utils.dart';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:squadre/utils/Firebase.dart' as fbUtils;

import 'TeamSelection.dart';

class UpdateProfile extends StatefulWidget {
  @override
  _UpdateProfileState createState() => _UpdateProfileState();
}

class _UpdateProfileState extends State<UpdateProfile> {
  File image;
  bool isEmailNotification = Utils.registerModel.isEmailEnabled;
  bool isPushNotification = Utils.registerModel.isPushEnabled;

  Future getImage() async {
    File picture = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxWidth: 300.0, maxHeight: 500.0);
    if (this.mounted) {
      setState(() {
        image = picture;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          color: Utils.colorWhite,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              Container(
                height: 200,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(gradient: Utils.gradient),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 50, bottom: 30),
                      child: FractionalTranslation(
                          translation: Offset(0.0, 0.8),
                          child: Text(
                            "M O D I F I C A   P R O F I L O",
                            style: TextStyle(
                                color: Utils.colorWhite, fontSize: 17),
                          )),
                    ),
                    FractionalTranslation(
                      translation: Offset(0, .5),
                      child: FlatButton(
                          onPressed: () {
                            getImage();
                          },
                          child: CircleAvatar(
                            radius: 48,
                            backgroundColor: Utils.purpleColor,
                            //  backgroundImage: Image.file(image);
                            //child: Icon(Icons.add,color: Utils.colorWhite,size: 40,),

                            child: image == null
                                ? Icon(
                                    Icons.add,
                                    color: Utils.colorWhite,
                                    size: 40,
                                  )
                                : CircleAvatar(
                                    radius: 48,
                                    backgroundImage: AssetImage(image.path),
                                  ),
                          )),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 60),
                child: Text(
                  "Nome Cognome",
                  style: TextStyle(color: Utils.greyColor, fontSize: 30),
                ),
              ),
              Text(
                "Citta",
                style: TextStyle(color: Utils.greyColor),
              ),
              Card(
                elevation: 5,
                margin: EdgeInsets.only(top: 15),
                shape: Border.all(color: Utils.colorHint),
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, bottom: 25, top: 25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "La mia squadra del cuore è",
                        style: TextStyle(color: Utils.buttonColor),
                      ),
                      Column(
                        children: <Widget>[
                          FlatButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => TeamSelection(
                                              isEditProfile: true,
                                            )));
                              },
                              child: Card(
                                elevation: 5,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(100)),
                                child: CircleAvatar(
                                  radius: 35,
                                  child: Image.asset('assets/images/' +Utils.registerModel.favoriteTeamName+ '.png',
                                    fit: BoxFit.scaleDown,
                                    height: 100,
                                  ),
                                  backgroundColor: Colors.white,
                                ),
                              )),
                          Text(
                            Utils.registerModel.favoriteTeamName ?? "-",
                            style: TextStyle(fontSize: 18),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10, right: 10, top: 15),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Notification via email"),
                        Switch(
                          value: isEmailNotification,
                          activeColor: Utils.pinkColor,
                          onChanged: (value) {
                            if (this.mounted) {
                              setState(() {
                                isEmailNotification = value;
                              });
                            }
                          },
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Push Notification"),
                        Switch(
                          value: isPushNotification,
                          activeColor: Utils.pinkColor,
                          onChanged: (value) {
                            if (this.mounted) {
                              setState(() {
                                isPushNotification = value;
                              });
                            }
                          },
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Utils.purpleColor,
        onPressed: () {
          if (this.mounted) {
            if (image != null) {
              _uploadImage(image);
            } else {
              fbUtils.setUserNotificationAndProfile(
                  null, isPushNotification, isEmailNotification);
            }
            Navigator.pop(context, true);
          }
        },
        child: Icon(
          Icons.done,
          color: Utils.colorWhite,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Future<String> _uploadImage(File image) async {
    var uuid = Utils.registerModel.uuid;
    var ext = image.path.substring(image.path.lastIndexOf('.'));
    final StorageReference ref = FirebaseStorage.instance
        .ref()
        .child('profilePictures')
        .child('$uuid$ext');
    final StorageUploadTask uploadTask = ref.putFile(image);
    uploadTask.events.listen((onData) async {
      final complete = uploadTask.isComplete;

      if (complete) {
        String url = await uploadTask.lastSnapshot.ref.getDownloadURL();
        fbUtils.setUserNotificationAndProfile(
            url, isPushNotification, isEmailNotification);
        return url;
      }

      return null;
    }, onError: (err) {
      print('_uploadImageAndSave end with error');
      return null;
    });
  }
}
