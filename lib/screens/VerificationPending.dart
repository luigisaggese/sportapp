import 'package:flutter/material.dart';
import 'package:squadre/utils/Utils.dart';
import 'package:squadre/utils/Firebase.dart' as fbUtils;

import 'Login.dart';

class VerificationPending extends StatefulWidget {
  @override
  _VerificationPendingState createState() => _VerificationPendingState();
}

class _VerificationPendingState extends State<VerificationPending> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Verifica la tua email', textAlign: TextAlign.center),
        backgroundColor: Utils.pinkColor,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          color: Utils.colorWhite,
          onPressed: () {
            //check if no
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        Login()),
                    (Route<dynamic> route) => false);
          },
        ),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 30.0, bottom: 30),
            child: Image.asset('assets/images/football.png'),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30.0, bottom: 30, left:20, right: 20),
            child: Text(
              "Siamo in attesa di verificare la tua email",
              style: TextStyle(fontSize: 35),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30.0, bottom: 30, left:10, right: 10),
            child: Text(
              "Procedi all'attivazione del tuo account! Clicca sul link che ti abbiamo inviato per email.",
              style: TextStyle(fontSize: 22),
            ),
          ),
          Container(
            width: 300,
            height: 50,
            margin: EdgeInsets.only(bottom: 20, top: 20, left:10, right: 10),
            child: RaisedButton(
                onPressed: () {
                  fbUtils.resendVerificationLink()
                      .then((status){
                    Utils.showMessage("Email di verifica inviato nuovamente!");
                  })
                      .catchError((error){
                    Utils.showMessage(error.message);
                  });
                },
                child: Text(
                  "Reinvia l'email di verifica",
                  style: TextStyle(color: Utils.colorWhite, fontSize: 20.0),
                ),
                color: Utils.buttonColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0))),
          ),
        ],
      ),
    );
  }
}
