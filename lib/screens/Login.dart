import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:squadre/screens/Register.dart';
import 'package:squadre/screens/TeamSelection.dart';
import 'package:squadre/utils/AppConstant.dart' as AppConstants;
import 'package:squadre/utils/Firebase.dart' as firebaseService;
import 'package:squadre/utils/Utils.dart';
import '../AccountNotVerifiedException.dart';
import 'ChatListPage.dart';
import 'ForgotPassword.dart';
import 'HomePage.dart';
import 'OnBoarding.dart';
import 'VerificationPending.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController nameController =
      TextEditingController(text: "luigisag@gmail.com");
  TextEditingController passController = TextEditingController(text: "123456");

  @override
  Widget build(BuildContext context) {
    Utils.deviceWidth = MediaQuery.of(context).size.width;
    Utils.deviceHeight = MediaQuery.of(context).size.height;
    EdgeInsets _padding =
        const EdgeInsets.symmetric(horizontal: 20.0, vertical: 8.0);
    bool passwordVisible = false;

    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: Utils.deviceWidth,
        color: Utils.colorWhite,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 100.0),
                child: Image.asset('assets/images/football.png'),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Text(
                  "Login",
                  style: TextStyle(fontSize: 35),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 30, left: 30, right: 30),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    TextField(
                        decoration: InputDecoration(
                            hintText: "Inserisci la tua email",
                            hintStyle: TextStyle(fontSize: 20),
                            labelText: "Email",
                            suffixIcon: Icon(
                              Utils.validateEmail(nameController.text)
                                  ? Icons.clear
                                  : Icons.check,
                              color: Theme.of(context).primaryColorDark,
                              size: 20.0,
                            ),
                            contentPadding:
                                new EdgeInsets.fromLTRB(0, 0, 0, -2),
                            labelStyle: TextStyle(
                                color: Utils.blackColor, fontSize: 20),
                            border: UnderlineInputBorder(
                                borderSide: BorderSide(color: Utils.greyColor)),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.grey,
                                    style: BorderStyle.solid))),
                        controller: nameController,
                        style: TextStyle(fontSize: 23)),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                            hintText: "Inserisci la tua password",
                            hintStyle: TextStyle(fontSize: 20),
                            labelText: "Password",
                            contentPadding:
                                new EdgeInsets.fromLTRB(0, 0, 0, -4),
                            labelStyle: TextStyle(
                                color: Utils.blackColor, fontSize: 20),
                            border: UnderlineInputBorder(
                                borderSide: BorderSide(color: Utils.greyColor)),
                            focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: Utils.greyColor))),
                        style: TextStyle(
                          fontSize: 23,
                        ),
                        controller: passController),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ForgotPassword()));
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(8.0, 8.0, 0.0, 8.0),
                        child: Text(
                          "Password dimenticata ?",
                          style:
                              TextStyle(color: Utils.greyColor, fontSize: 18),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                width: 300,
                height: 50,
                margin: EdgeInsets.only(bottom: 30, top: 40),
                child: RaisedButton(
                    onPressed: () {
                      login(nameController.text, passController.text);
                    },
                    child: Text(
                      "Login",
                      style: TextStyle(color: Utils.colorWhite, fontSize: 20.0),
                    ),
                    color: Utils.buttonColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0))),
              ),
              Text(
                "oppure",
                style: TextStyle(color: Utils.greyColor),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 20,
                  bottom: 20,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    GestureDetector(
                      child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(100)),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Image.asset(
                              "assets/images/google.png",
                              scale: 2,
                            ),
                          )),
                      onTap: () {
                        loginViaGoogle();
                      },
                    ),
                    GestureDetector(
                      child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(100)),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Image.asset(
                              "assets/images/fb.png",
                              scale: 2,
                            ),
                          )),
                      onTap: () {
                        fbLogin();
                      },
                    ),
                    GestureDetector(
                      child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(100)),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Image.asset(
                              "assets/images/twitter.png",
                              scale: 2,
                            ),
                          )),
                      onTap: () {
                        twitterLogin();
                      },
                    ),
                    GestureDetector(
                      child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(100)),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Image.asset(
                              "assets/images/instagram.png",
                              scale: 2,
                            ),
                          )),
                      onTap: _instagramLogin,
                    ),
                  ],
                ),
              ),
              FlatButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Register()));
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Non hai un account ?",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold)),
                      SizedBox(
                        width: 12,
                      ),
                      Text(
                        "Registrati",
                        style: TextStyle(color: Utils.lightBlueColor),
                      )
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }

  void login(String email, String password) {
    firebaseService.login(email, password).then((value) {
      if (value != null) {
//        SharedPref().setStringShared("login", "true");
        //set uid in shared preference so that questions posted by this id only will be displayed
        //      SharedPref().setStringShared("uid", value);
        Utils.registerModel.uuid = value;
        _insertOrUpdate();
      } else {
        print(Exception);
      }
    }).catchError((error) {
      if (error is AccountNotVerifiedException) {
        Utils.showMessage("Activation link sent to your registered email!");
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => VerificationPending()));
      } else {
        Utils.showMessage(Utils.getTranslation("invalidUserNPass"));
      }

      print(error);
    });
  }

  void loginViaGoogle() {
    try {
      Utils.api.googleInfo().then((b) {
        if (b) {
          _insertOrUpdate();
        }
      }).catchError((onError) {
        print(onError);
      });
    } catch (e) {
      print(e);
    }
  }

  void fbLogin() {
    try {
      Utils.api.facebookInfo().then((b) {
        if (b) {
          _insertOrUpdate();
        }
      }).catchError((onError) {
        print(onError);
      });
    } catch (e) {
      print(e);
    }
  }

  void twitterLogin() {
    try {
      Utils.api.twitterInfo().then((b) {
        if (b) {
          _insertOrUpdate();
        }
      }).catchError((onError) {
        print(onError);
      });
    } catch (e) {
      print(e);
    }
  }

  _instagramLogin() {
    Utils.api.instagramInfo().then((success) {
      if (success) {
        _insertOrUpdate();
      }
    }).catchError((error) {
      Utils.showMessage(error.errorMessage);
      print(error);
    });
  }

  _insertOrUpdate() {
    firebaseService
        .getProfile(Utils.registerModel.uuid)
        .then((documentReference) {
      if (documentReference.exists) {
        Utils.registerModel.isAdmin =
            documentReference.data[AppConstants.IS_ADMIN];
        Utils.registerModel.isPushEnabled =
            documentReference.data[AppConstants.IS_PUSH_ENABLED];
        Utils.registerModel.isEmailEnabled =
            documentReference.data[AppConstants.IS_EMAIL_ENABLED];

        Utils.registerModel.name = documentReference.data[AppConstants.NAME];
        Utils.registerModel.email = documentReference.data[AppConstants.EMAIL];
        Utils.registerModel.favoriteTeamName =
            documentReference.data[AppConstants.FAVORITE_TEAM_NAME];
        Utils.registerModel.avatar =
            documentReference.data[AppConstants.AVATAR];
        Utils.registerModel.surname =
            documentReference.data[AppConstants.SURNAME];
        Utils.registerModel.questionCounter =
            documentReference.data[AppConstants.QUESTION_COUNTER];
        var questionTimeout =
            documentReference.data[AppConstants.QUESTION_TIMEOUT] ;
        if (questionTimeout != null) {
          Utils.registerModel.questionTimeout = (questionTimeout as Timestamp).toDate();
        }
        Utils.registerModel.isVerified = true;
        Utils.saveUserData();

        if (Utils.registerModel.isAdmin == true) {
          Utils.showMessage("Hello Admin");
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => ChatListPage()));
        } else {
          Utils.showMessage("Welcome user");

          if (Utils.registerModel.favoriteTeamName.isEmpty) {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => TeamSelection()));
          } else {
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => HomePage()));
          }
        }
      } else {
        firebaseService.userDetails(Utils.registerModel);

        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => OnBoardingPage()));
      }
    });
  }
}
