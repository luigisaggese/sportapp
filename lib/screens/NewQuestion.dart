import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:squadre/utils/Firebase.dart' as firebaseService;
import 'package:squadre/utils/SharedPref.dart';
import 'package:squadre/utils/Utils.dart';

class NewQuestion extends StatefulWidget {
  @override
  _NewQuestionState createState() => _NewQuestionState();
}

class _NewQuestionState extends State<NewQuestion> {
  TextEditingController controller;

  String teamSelected = Utils.registerModel.favoriteTeamName;
  var teamList = Utils.getTeamList();
  Duration initialtimer = new Duration();

  @override
  void initState() {
    super.initState();
    controller = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Card(
        elevation: 10,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
        child: FloatingActionButton(
          backgroundColor: Utils.purpleColor,
          onPressed: () {
            addQuestion();
          },
          child: Icon(
            Icons.done,
            color: Utils.colorWhite,
          ),
        ),
      ),
      body: SingleChildScrollView(
          child: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            AppBar(
              elevation: 0,
              backgroundColor: Utils.pinkColor,
              leading: IconButton(
                icon: Icon(Icons.arrow_back_ios),
                color: Utils.colorWhite,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
            Container(
              height: 60.0,
              width: Utils.deviceWidth,
              color: Utils.pinkColor,
              alignment: Alignment.center,
              child: Text(
                'Su quale squadra vuoi\n farela tua domanda?',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 200.0,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Container(
                    width: 150.0,
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Material(
                            shape: RoundedRectangleBorder(
                                side: BorderSide(
                                    color: teamList[index] == teamSelected
                                        ? Utils.blueColor
                                        : Utils.greyColor,
                                    width: 3.0),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: GestureDetector(
                                  onTap: () {
                                    if (this.mounted) {
                                      setState(() {
                                        teamSelected = teamList[index];
                                      });
                                    }
                                  },
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Image.asset(
                                          'assets/images/' +
                                              teamList[index] +
                                              '.png',
                                          fit: BoxFit.scaleDown,
                                          height: 100,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 10),
                                        child: Text(
                                          teamList[index],
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      )
                                    ],
                                  )),
                            ),
                            elevation: 20.0,
                            shadowColor: Utils.blueShadowColor,
                          ),
                        ),
                      ],
                    ),
                  );
                },
                itemCount: teamList.length,
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.only(bottom: 50),
              height: 320.0,
              width: Utils.deviceWidth - 20,
              child: Material(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0))),
                elevation: 20.0,
                color: Colors.white,
                shadowColor: Utils.blueShadowColor,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: TextField(
                    controller: controller,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Scrivi la tua domanda…',
                      contentPadding: EdgeInsets.only(left: 15, top: 15),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      )),
    );
  }


  void addQuestion() {
    if (controller.text.trim().isEmpty) {
      Fluttertoast.showToast(
          msg: "La domanda non può essere vuota!",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }
    Utils.firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));

    Utils.firebaseMessaging.getToken().then((token) => firebaseService.setToken(token));

    //adds new question in firebase database
    firebaseService.addPost(controller.text, teamSelected)
        .then((_) {
        Utils.showMessage("Domanda effettuata");
        if (this.mounted) {
          setState(() {
            controller.text = "";
          });
        }
        Utils.refresh.add(true);
        Navigator.pop(context);
      });
  }

}
