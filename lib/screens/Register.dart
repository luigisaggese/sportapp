import 'package:flutter/material.dart';
import 'package:squadre/utils/Utils.dart';
import 'package:squadre/utils/Firebase.dart' as fbUtils;

import 'VerificationPending.dart';

class Register extends StatefulWidget {

  Register();

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  TextEditingController nameController = TextEditingController();
  TextEditingController surnameController = TextEditingController();
  TextEditingController passController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        color: Utils.colorWhite,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 100.0),
                child: Image.asset('assets/images/football.png'),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Text(
                  "Registrazione",
                  style: TextStyle(fontSize: 35),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 30, left: 10, right: 10),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    TextField(
                      decoration: InputDecoration(
                          hintText: "Inserisci indirizzo Email",
                          hintStyle: TextStyle(fontSize: 20),
                          labelText: "Email",
                          suffixIcon: Icon(
                            Utils.validateEmail(nameController.text.trim())
                                ? Icons.clear
                                : Icons.check,
                            color: Theme.of(context).primaryColorDark,
                            size: 20.0,
                          ),
                          labelStyle: TextStyle(color: Utils.blackColor),
                          border: UnderlineInputBorder(
                              borderSide: BorderSide(color: Utils.greyColor))),
                      controller: emailController,
                      style: TextStyle(fontSize: 23),
                    ),
                    TextField(
                      decoration: InputDecoration(
                          hintText: "Inserisci il tuo nome",
                          hintStyle: TextStyle(fontSize: 20),
                          labelText: "Nome",
                          labelStyle: TextStyle(color: Utils.blackColor),
                          border: UnderlineInputBorder(
                              borderSide: BorderSide(color: Utils.greyColor))),
                      controller: nameController,
                      style: TextStyle(fontSize: 23),
                    ),
                    TextField(
                      decoration: InputDecoration(
                          hintText: "Inserisci il tuo cognome",
                          hintStyle: TextStyle(fontSize: 20),
                          labelText: "Cognome",
                          labelStyle: TextStyle(color: Utils.blackColor),
                          border: UnderlineInputBorder(
                              borderSide: BorderSide(color: Utils.greyColor))),
                      controller: surnameController,
                      style: TextStyle(fontSize: 23),
                    ),
                    TextField(
                      decoration: InputDecoration(
                          hintText: "Inserisci una password",
                          hintStyle: TextStyle(fontSize: 20),
                          labelText: "Password",
                          errorText: length != null ? 'La password non può essere vuota o minore di 6 caratteri' : null,
                          labelStyle: TextStyle(color: Utils.blackColor),
                          border: UnderlineInputBorder(
                              borderSide: BorderSide(color: Utils.greyColor))),
                      controller: passController,
                      style: TextStyle(fontSize: 23),
                    )
                  ],
                ),
              ),
              Container(
                width: 300,
                height: 50,
                margin: EdgeInsets.only(top: 30),
                child: RaisedButton(
                    onPressed: () {
                      register();
                    },
                    child: Text(
                      "Registrazione",
                      style: TextStyle(color: Utils.colorWhite, fontSize: 20),
                    ),
                    color: Utils.buttonColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0))),
              ),
              SizedBox(height: 16,)
            ],
          ),
        ),
      ),
    );
  }

  void register() {
    Utils.registerModel.name = nameController.text;

    fbUtils.register(emailController.text.trim(), passController.text, nameController.text.trim(), surnameController.text.trim()).then((value) {
      if (value) {
        Utils.registerModel.isVerified = false;
        Utils.showMessage("Activation link sent to your registered email!");
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => VerificationPending()));
      } else {
        print(Exception);
      }
    }).catchError((error) {
      print(error);
      Utils.showMessage(error.message);
    });
  }
  bool length(String value) {
    bool isMatch = false;
    if (value.length >= 6) {
      isMatch = true;
    }
    return isMatch;
  }

}
