import 'dart:io';
import 'package:flutter_ffmpeg/flutter_ffmpeg.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:squadre/utils/Firebase.dart' as firebaseService;
import 'package:squadre/utils/Utils.dart';
import 'package:squadre/widgets/ChatMessages.dart';
import 'package:file_picker/file_picker.dart';
import 'package:uuid/uuid.dart';

class ChatPage extends StatefulWidget {
  String question, username, image, postUUId;

  ChatPage(this.question, this.username, this.image, this.postUUId);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  bool isUploading = false;
  List<Widget> list = [];

  TextEditingController replyController;

  @override
  void initState() {
    super.initState();
    list.add(new Bubble(
      message: widget.question,
      time: "",
      delivered: true,
      isMe: true,
      isVideo: false,
    ));
    replyController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Utils.colorAppBar,
        elevation: 0,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Utils.blueColor,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(padding: const EdgeInsets.only(left: 8.0)),
            CircleAvatar(
                radius: 20.0,
                backgroundColor: Colors.white,
                child: ClipOval(child: Image.network(widget.image))),
            Text(widget.username)
          ],
        ),
      ),
      body: Container(
        child: Stack(
          children: <Widget>[
            Container(
                child: isUploading
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : ListView(
                        children: list,
                      )),
            Utils.registerModel.isAdmin
                ? Align(
                    alignment: Alignment.bottomLeft,
                    child: BottomAppBar(
                      color: Utils.colorAppBar,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 5, top: 5),
                        child: Row(
                          children: <Widget>[
                            IconButton(
                              onPressed: () {
                                _pickVideo(context);
                              },
                              icon: Icon(
                                Icons.add,
                                color: Utils.blueColor,
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30.0),
                                  color: Utils.colorWhite),
                              width: MediaQuery.of(context).size.width - 105,
                              child: TextField(
                                controller: replyController,
                                decoration:
                                    InputDecoration(border: InputBorder.none),
                              ),
                            ),
                            IconButton(
                              onPressed: () {
                                firebaseService.addPostReply(
                                    replyController.text,
                                    Utils.registerModel.uuid,
                                    widget.postUUId,
                                    false);
                                setState(() {
                                  list.add(Bubble(
                                    message: replyController.text.toString(),
                                    time: DateTime.now().toIso8601String(),
                                    delivered: true,
                                    isMe: true,
                                    isVideo: false,
                                  ));
                                  replyController.text = '';
                                });

                              },
                              icon: Icon(
                                Utils.registerModel.isAdmin
                                    ? Icons.send
                                    : Icons.send,
                                color: Utils.blueColor,
                              ),
                            ),
//                      IconButton(
//                        onPressed: () {},
//                        icon: Icon(
//                          Icons.settings_voice,
//                          color: Utils.blueColor,
//                        ),
//                      ),
                          ],
                        ),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }

  Future<void> _pickVideo(BuildContext context) async {
    if (mounted) {
      print('_pickVideo start');

      var path = await FilePicker.getFilePath(type: FileType.VIDEO);
      print('compression start');
      var newPath = await _processarVideo(context, path);
/*
      _image = await _flutterVideoCompress
          .getThumbnail(path: path, quality: 50)
          .whenComplete(() {
        setState(() {      isUploading = true;});
      });
      final String newPath = await _flutterVideoCompress.startCompress(
        path: path,
        deleteOrigin: true,
      );
      */
      print('compression end');

      _uploadFile(newPath);
      print(newPath);
      print('_pickVideo end');
    }
  }

  Future<String> _processarVideo(BuildContext context, String path) async {
    try {
      File file = File(path);

      if (!file.existsSync()) {
        throw Exception("Nenhum arquivo foi encontrado!");
      }

      String newFileName = "${DateTime.now().millisecondsSinceEpoch}_";

      newFileName += "h264_${withoutExtension(basename(path))}";

      newFileName += extension(path);

      File newFile = File(join(file.parent.path, newFileName));

      final FlutterFFmpeg _flutterFFmpeg = FlutterFFmpeg();

      List<String> arguments = [
        "-i", //Entrada
        file.path,
        "-c:v", //Codec
        "mpeg4",
        newFile.path,
      ];

      _flutterFFmpeg.getExternalLibraries();

      await _flutterFFmpeg.executeWithArguments(arguments);

      int output = await _flutterFFmpeg.getLastReturnCode();

      if (output != 0) {
        throw Exception(await _flutterFFmpeg.getLastCommandOutput());
      }
      return newFile.path;
    } catch (e) {
      print(e);
    }
    return null;
  }

  _uploadFile(String filePath) async {
    print('_uploadFile start');

    var name = basename(filePath);
    var index = name.lastIndexOf('.');
    var ext = name.substring(index);
    final String uuid = Uuid().v1();
    final Directory systemTempDir = Directory.systemTemp;
    final File file = await File(filePath).create();

    final StorageReference ref =
        FirebaseStorage.instance.ref().child('media').child('vid_$uuid$ext');
    final StorageUploadTask uploadTask = ref.putFile(file);
    uploadTask.events.listen((onData) async {
      final complete = uploadTask.isComplete;
      if (complete) {
        if (this.mounted) {
          setState(() {
            isUploading = false;
          });
        }
        String url = await _getUrl(uploadTask);
        print('upload video url $url');

        firebaseService.addPostReply(
            url, Utils.registerModel.uuid, widget.postUUId, true);
        setState(() {
          list.add(Bubble(
            message: url,
            time: DateTime.now().toIso8601String(),
            delivered: true,
            isMe: false,
            isVideo: true,
          ));
        });
      }
      print('_uploadFile end');

      return null;
    }, onError: (err) {
      if (this.mounted) {
        setState(() {
          isUploading = false;
        });
      }
      print('_uploadFile end with error');
      return null;
    });
  }

  _getUrl(StorageUploadTask task) async {
    return await task.lastSnapshot.ref.getDownloadURL();
  }
}
