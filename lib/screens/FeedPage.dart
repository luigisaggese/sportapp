import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:squadre/utils/Firebase.dart' as firebaseService;
import 'package:squadre/utils/Utils.dart';
import 'package:squadre/utils/AppConstant.dart' as AppConstant;
import 'package:squadre/widgets/PostItem.dart';

class FeedPage extends StatefulWidget {
  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage> {
  int page = 0;

  List postList;

  bool isFiltered = false;
  bool _isloading = false;

  @override
  void initState() {
    super.initState();

    Utils.refresh.listen((b) {
      print('Refresh $b');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Utils.colorWhite,
      appBar: AppBar(
        backgroundColor: Utils.colorWhite,
        elevation: 5,
        centerTitle: true,
        title: GestureDetector(
          onTap: () {},
          child: Text(
            'R I S P O S T E',
            style: TextStyle(
                color: Utils.textColor,
                fontWeight: FontWeight.bold,
                fontSize: 20.0),
          ),
        ),
        actions: <Widget>[
          new Container(
            margin: const EdgeInsets.all(10.0),
            decoration: new BoxDecoration(
                border: new Border.all(
                    color: isFiltered ? Colors.black : Colors.transparent),
                shape: BoxShape.circle),
            child: GestureDetector(
              onTap: () async {
                setState(() {
                  isFiltered = !isFiltered;
                });
              },
              child: Image.asset(
                'assets/images/' +
                    Utils.registerModel.favoriteTeamName +
                    '.png',
                fit: BoxFit.scaleDown,
                height: 30,
              ),
            ),
          )
        ],
      ),
      body: _isloading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : new StreamBuilder<QuerySnapshot>(
              stream: firebaseService.getPosts(),
              //sort: (b, a) => a.value["time"].compareTo(b.value["time"]),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (!snapshot.hasData) return const Text('Loading...');
                final int messageCount = snapshot.data.documents.length;
                return new ListView.builder(
                    itemCount: messageCount,
                    itemBuilder: (_, int index) {
                      var document = snapshot.data.documents[index];
                      var emptyContainer = new Container();
                      var gestureDetector = GestureDetector(
                        child: PostItem(documentSnapshot: document),
                        onTap: () {},
                      );
                      if (document[AppConstant.POST_IS_REPLY] != null) {
                        if (isFiltered) {
                          if (document[AppConstant.TEAM_NAME] ==
                              Utils.registerModel.favoriteTeamName) {
                            return gestureDetector;
                          } else {
                            return emptyContainer;
                          }
                        } else {
                          return gestureDetector;
                        }
                      }
                      return emptyContainer;
                    });
              }),
    );
  }
}
