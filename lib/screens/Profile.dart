import 'package:flutter_advanced_networkimage/src/flutter_advanced_networkimage.dart';

import 'package:flutter/material.dart';
import 'package:squadre/screens/TeamSelection.dart';
import 'package:squadre/utils/SharedPref.dart';
import 'package:squadre/utils/Utils.dart';
import 'Login.dart';
import 'UpdateProfile.dart';
import 'package:squadre/utils/AppConstant.dart' as AppConstant;

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> with AutomaticKeepAliveClientMixin {

  @override // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          color: Utils.colorWhite,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              Container(
                height: 200,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(gradient: Utils.gradient),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 60, bottom: 20),
                      child: FractionalTranslation(
                        translation: Offset(0.0, 0.6),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            GestureDetector(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20.0),
                                child: Icon(
                                  Icons.power_settings_new,
                                  color: Utils.colorWhite,
                                ),
                              ),
                              onTap: () {
                                SharedPref().clear();
                                Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            Login()),
                                    (Route<dynamic> route) => false);
                              },
                            ),
                            Container(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "P R O F I L O",
                                  style: TextStyle(
                                      color: Utils.colorWhite, fontSize: 17),
                                ),
                              ],
                            )),
                            GestureDetector(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 25.0),
                                child: Icon(
                                  Icons.edit,
                                  color: Utils.colorWhite,
                                ),
                              ),
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => UpdateProfile()));
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                    FractionalTranslation(
                        translation: Offset(0, .5),
                        child: FlatButton(
                            onPressed: () {},
                            child: CircleAvatar(
                              radius: 48,
                              backgroundImage: AdvancedNetworkImage(
                                  getImageProfile(),
                                  useDiskCache: true),
                            )))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 60,
                ),
                child: Text(
                  getUsername(),
                  style: TextStyle(color: Utils.buttonColor, fontSize: 30),
                ),
              ),
              Text(
                "",
                style: TextStyle(color: Utils.buttonColor, fontSize: 18),
              ),
              Card(
                elevation: 3,
                color: Color(0xFFf9f9f9),
                margin: EdgeInsets.only(top: 20, left: 5, right: 5),
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, bottom: 25, top: 25),
                  child: Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(Utils.registerModel.questionCounter.toString()),
                            Text(
                              "Domande fatte",
                              style: TextStyle(
                                  color: Utils.greyColor, fontSize: 12),
                            )
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(Utils.registerModel.questionTimeout.difference(DateTime.now()).inHours.toString() +
                                " hours " +
                                Utils.registerModel.questionTimeout.difference(DateTime.now()).inMinutes.toString() +
                                " min " +
                                Utils.registerModel.questionTimeout.difference(DateTime.now()).inSeconds.toString() +
                                " seconds"),
                            Text(
                              "Tempo mancante per la prossima domanda",
                              style: TextStyle(
                                  color: Utils.greyColor, fontSize: 12),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Card(
                elevation: 3,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, bottom: 25, top: 25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "La mia squadra del cuore è",
                        style:
                            TextStyle(color: Utils.buttonColor, fontSize: 20),
                      ),
                      Column(
                        children: <Widget>[
                          FlatButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => TeamSelection()));
                              },
                              child: Card(
                                elevation: 5,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(100)),
                                child: CircleAvatar(
                                  radius: 35,
                                  child: Image.asset('assets/images/' +Utils.registerModel.favoriteTeamName+ '.png',
                                    fit: BoxFit.scaleDown,
                                    height: 100,
                                  ),
                                  backgroundColor: Colors.white,
                                ),
                              )),
                          Text(
                            Utils.registerModel.favoriteTeamName ?? "-",
                            style: TextStyle(fontSize: 18),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }


  String getImageProfile() {
    if (Utils.registerModel.avatar == null) {
      return AppConstant.DEFAULT_AVATAR;
    }
    if (Utils.registerModel.avatar.contains("graph.facebook.com")) {
      return Utils.registerModel.avatar + "?type=large";
    }
    if (Utils.registerModel.avatar.contains("twimg")) {
      return Utils.registerModel.avatar.replaceAll("_normal", "");
    }
    return Utils.registerModel.avatar;
  }

  String getUsername() {
    String retValue = '';
    if (Utils.registerModel.name != null) {
      retValue = Utils.registerModel.name;
    }
    if (Utils.registerModel.surname != null) {
      retValue = retValue + ' ' + Utils.registerModel.surname;
    }
    return retValue;
  }
}
