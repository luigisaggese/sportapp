import 'package:flutter/material.dart';
import 'package:squadre/utils/Firebase.dart' as fbUtils;
import 'package:squadre/utils/Utils.dart';
import 'package:squadre/widgets/TeamItems.dart';

import 'HomePage.dart';

class TeamSelection extends StatefulWidget {
  final bool
      isEditProfile; //if its true this means TeamSelection screen open from edit profile. so redirect user back to edit profile.

  const TeamSelection({Key key, this.isEditProfile = false}) : super(key: key);
  @override
  _TeamSelectionState createState() => _TeamSelectionState();
}

class _TeamSelectionState extends State<TeamSelection> {
  String teamSelected = Utils.registerModel.favoriteTeamName;

  var teamList = Utils.getTeamList();

  @override
  void initState() {
    super.initState();
    if(Utils.registerModel.favoriteTeamName == null || Utils.registerModel.favoriteTeamName.isEmpty){
      teamSelected = "juventus";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Utils.colorWhite,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
              return Container(
                color: Utils.colorWhite,
                margin: EdgeInsets.only(bottom: 15),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 40.0),
                      child: Text("Quale e la tua\nSquarda del cuore?",
                          textAlign: TextAlign.center,
                          style:
                              TextStyle(fontSize: 35, color: Utils.blackColor)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text(
                        "Per restare sempre informato",
                        style: TextStyle(color: Utils.textColor, fontSize: 18),
                      ),
                    ),
                    Text(
                      "sulla tua squadra. ",
                      style: TextStyle(color: Utils.textColor, fontSize: 18),
                    )
                  ],
                ),
              );
            }, childCount: 1),
          ),
          SliverPadding(padding: EdgeInsets.only(top: 20.0)),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, childAspectRatio: 1),
            delegate: SliverChildBuilderDelegate((context, index) {
              return Padding(
                padding: const EdgeInsets.all(5.0),
                child: TeamItems(
                  teamList[index],
                  isSelected: teamList[index] == teamSelected,
                  callback: (b) {
                    teamSelected = teamList[index];

                    if (this.mounted) {
                      setState(() {});
                    }
                  },
                ),
              );
            }, childCount: teamList.length),
          ),
          SliverPadding(padding: EdgeInsets.only(top: 100.0)),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Utils.purpleColor,
        elevation: 20,
        onPressed: () {
          fbUtils.setTeam(teamSelected).then((data) {
            if (widget.isEditProfile) {
              Navigator.pop(context,
                  true); //passing true to reload UpdateProfile screen to display newly selected Team info.
            } else {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => HomePage()),
                  (Route<dynamic> route) => false);
            }
          });
        },
        child: Icon(
          Icons.done,
          color: Utils.colorWhite,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
