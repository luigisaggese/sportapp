import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:squadre/screens/Login.dart';
import 'package:squadre/utils/SharedPref.dart';
import 'package:squadre/utils/Utils.dart';
import 'package:squadre/utils/AppConstant.dart' as AppConstant;
import 'dart:async';
import 'package:squadre/utils/Firebase.dart' as fbUtils;

import 'ChatListPage.dart';
import 'HomePage.dart';
import 'VerificationPending.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Utils.deviceWidth = MediaQuery.of(context).size.width;
    Utils.deviceHeight = MediaQuery.of(context).size.height;

    Timer(Duration(seconds: 2), () async {
      final _auth = await FirebaseAuth.instance;
      final FirebaseUser currentUser = await _auth.currentUser();

      Utils.readUserData().then((v) {
        if (Utils.registerModel.email == null ||
            currentUser == null ||
            currentUser.email == null) {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => Login()));
        } else if (Utils.registerModel?.isAdmin != null &&
            Utils.registerModel.isAdmin) {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => ChatListPage()));
        } else {
          if (Utils.registerModel.isVerified) {
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => HomePage()));
          } else {
            fbUtils.getProfile(currentUser.uid).then((documentReference) {
              if (documentReference.exists &&
                  !documentReference.data[AppConstant.IS_ADMIN] &&
                  (documentReference.data[AppConstant.FAVORITE_TEAM_NAME] ==
                          null ||
                      documentReference.data[AppConstant.FAVORITE_TEAM_NAME]
                          .toString()
                          .isEmpty)) {
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => VerificationPending()));
              } else {
                SharedPref().clear();
                Navigator.pushReplacement(
                    context, MaterialPageRoute(builder: (context) => Login()));
              }
            });
          }
        }
      });
    });

    return Scaffold(
      body: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Positioned.fill(
            child: Image.asset(
              'assets/images/splash.png',
              fit: BoxFit.fitWidth,
              height: 10,
              alignment: Alignment(0, 1),
            ),
          ),
          Container(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 100.0),
                  child: Image.asset('assets/images/football.png'),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Text(
                    "Domande\nrisposte",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 45, color: Utils.blackColor),
                  ),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: Text(
                "",
                style: TextStyle(fontSize: 20),
              ),
            ),
          )
        ],
      ),
    );
  }
}
