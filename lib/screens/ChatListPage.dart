import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:squadre/screens/SplashScreen.dart';
import 'package:squadre/utils/AppConstant.dart' as AppConstant;
import 'package:squadre/utils/Firebase.dart' as firebaseService;
import 'package:squadre/utils/Utils.dart';
import 'package:squadre/widgets/ChatItem.dart';
import 'package:squadre/widgets/TeamItems2.dart';

class ChatListPage extends StatefulWidget {
  @override
  _UsersListState createState() => _UsersListState();
}

class _UsersListState extends State<ChatListPage> {
  List teamList = Utils.getTeamList();
  Map teamListData;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new StreamBuilder<QuerySnapshot>(
          stream: firebaseService.getPosts(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              return new Container(
                  color: Utils.colorWhite,
                  child: CustomScrollView(
                    slivers: <Widget>[
                      SliverList(
                        delegate: SliverChildBuilderDelegate((context, index) {
                          return GestureDetector(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 30, top: 50, bottom: 20),
                              child: Text(
                                "Squadre",
                                style: TextStyle(
                                    color: Utils.greyColor, fontSize: 16),
                              ),
                            ),
                            onTap: () async {
                              SharedPreferences sharedPreferences =
                                  await SharedPreferences.getInstance();

                              sharedPreferences.clear();
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          SplashScreen()),
                                  (Route<dynamic> route) => false);
                            },
                          );
                        }, childCount: 1),
                      ),
                      SliverList(
                        delegate: SliverChildBuilderDelegate((context, index) {
                          return Column(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width,
                                height: 80,
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  itemCount: teamList.length,
                                  itemBuilder: (_, int index) {
                                    var count = snapshot.data.documents
                                        .where((d) {
                                          return d[AppConstant.TEAM_NAME] ==
                                                  teamList[index] &&
                                              d[AppConstant.POST_IS_REPLY] == null;
                                        })
                                        .toList()
                                        .length;
                                    if (count > 0) {
                                      return Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: TeamItems2(
                                            teamList[index], count, 30, 30,
                                            count: 1, callback: (b) {
                                          //TODO Onclick on team
                                          //selectTeam(index);
                                        }),
                                      );
                                    }
                                    return new Container();
                                    ;
                                  },
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Divider(height: 1),
                              )
                            ],
                          );
                        }, childCount: 1),
                      ),
                      SliverList(
                          delegate:
                              SliverChildBuilderDelegate((contesto, indice) {
                        return Column(children: <Widget>[
                          Container(
                              width: MediaQuery.of(contesto).size.width,
                              height: MediaQuery.of(contesto).size.height-100,
                              child: ListView.builder(
                                  itemCount: snapshot.data.documents.length,
                                  itemBuilder: (_, int index) {
                                    var gestureDetector = GestureDetector(
                                      child: ChatItem(
                                          snapshot.data.documents[index]),
                                      onTap: () {},
                                    );
                                    if (snapshot.data.documents[index]
                                            [AppConstant.POST_IS_REPLY] ==
                                        null) {
                                      return gestureDetector;
                                    }
                                    return new Container();
                                  }))
                        ]);
                      }, childCount: 1)),
                    ],
                  ));
            } else {
              return const Text('Loading...');
            }
          }),
    );
  }
}
