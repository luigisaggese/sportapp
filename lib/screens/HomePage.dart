import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:squadre/screens/FeedPage.dart';
import 'package:squadre/screens/NewQuestion.dart';
import 'package:squadre/screens/Profile.dart';
import 'package:squadre/utils/Utils.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int page = 0;

  @override
  void initState() {
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        height: 60,
        decoration: new BoxDecoration(
          color: Colors.white,
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 20.0),
              blurRadius: 40.0,
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
                icon: Icon(Icons.menu),
                onPressed: () {
                  if (this.mounted) {
                    setState(() {
                      page = 0;
                    });
                  }
                }),
            IconButton(
                icon: Icon(Icons.account_circle),
                onPressed: () {
                  if (this.mounted) {
                    setState(() {
                      page = 1;
                    });
                  }
                })
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Utils.purpleColor,
        elevation: 20,
        onPressed: () {
          if(Utils.registerModel.questionTimeout.difference(DateTime.now()).inSeconds<0){
            Navigator.push(

                context, MaterialPageRoute(builder: (context) => NewQuestion()));
          }else{
            Fluttertoast.showToast(
                msg: "Non è possibile effettuare una nuova domanda. Controlla il tuo profile per vedere quanto manca!",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIos: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0);

          }

        },
        child: Icon(
          Icons.add,
          color: Utils.colorWhite,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: getWidget(context),
    );
  }

  Widget getWidget(BuildContext context) {
    switch (page) {
      case 0:
        return FeedPage();
        break;

      case 1:
        return Profile();
    }

    return Container();
  }
}
