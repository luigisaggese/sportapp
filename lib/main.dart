import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:squadre/screens/SplashScreen.dart';
import 'package:squadre/utils/Utils.dart';
import 'package:squadre/utils/Firebase.dart' as fbUtils;
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

void main() async{

  Utils.readUserData();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp
  ]);
  SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);


  FlutterError.onError = (FlutterErrorDetails details) {
    Crashlytics.instance.onError(details);
  };
  Crashlytics.instance.enableInDevMode = true;
  // fbUtils.initializePlacePicker();

  Utils.firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print("onMessage");
        print(message);
      },
      onLaunch: (Map<String, dynamic> message) {
        print("onLaunch");
        print(message);
      },
      onResume: (Map<String, dynamic> message) {
        print("onResume");
        print(message);
      });

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static FirebaseAnalytics analytics = new FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer = new FirebaseAnalyticsObserver(analytics: analytics);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        fontFamily: 'CalibreMedium',
      ),
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      navigatorObservers: <NavigatorObserver>[observer],
    );
  }
}
