class UserModel {
  String uuid;

  String _favoriteTeamName, _avatar, _name, _surname;
  bool _isAdmin, _isEmailEnabled, _isPushEnabled;
  String email;
  bool _isVerified;
  int _questionCounter;
  DateTime _questionTimeout;

  UserModel();
  DateTime get questionTimeout => _questionTimeout;

  String get name => _name;
  int get questionCounter => _questionCounter;

  String get avatar => _avatar;

  String get favoriteTeamName => _favoriteTeamName;

  bool get isAdmin => _isAdmin;
  bool get isPushEnabled => _isPushEnabled;
  bool get isEmailEnabled => _isEmailEnabled;
  set questionTimeout(DateTime value) => _questionTimeout = value;

  set avatar(String value) => _avatar = value;
  set questionCounter(int value) => _questionCounter = value;

  set isAdmin(bool value) => _isAdmin = value;
  set isPushEnabled(bool value) => _isPushEnabled = value;
  set isEmailEnabled(bool value) => _isEmailEnabled = value;

  set name(String value) => _name = value;

  set favoriteTeamName(String value) => _favoriteTeamName = value;

  String get surname => _surname;
  set surname(String value) => _surname = value;

  bool get isVerified => _isVerified;
  set isVerified(bool value) => _isVerified = value;

}
