
class AccountNotVerifiedException implements Exception {
  final String cause;

  AccountNotVerifiedException(this.cause);
}