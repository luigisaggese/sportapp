import 'package:flutter/material.dart';
import 'package:squadre/utils/AppConstant.dart' as AppConstant;
import 'package:squadre/utils/Firebase.dart' as Database;

class ProfileTile extends StatefulWidget{

  String postUuid;
  ProfileTile(this.postUuid);

  @override
  _ProfileTileState createState() => new _ProfileTileState(postUuid);
}
class _ProfileTileState extends State<ProfileTile>  with AutomaticKeepAliveClientMixin{
  String postUuid;
  _ProfileTileState(this.postUuid);

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[
      Padding(padding: const EdgeInsets.only(left: 8.0)),
      CircleAvatar(
        radius: 20.0,
        backgroundColor: Colors.white,
        child: ClipOval(
          child: FutureBuilder(
              future: Database.getProfile(postUuid),
              builder: (context, AsyncSnapshot snapshot) =>
              (snapshot.hasData && snapshot.data != null && snapshot.data[AppConstant.AVATAR]!= null)
                  ? Image.network(snapshot.data[AppConstant.AVATAR])
                  : Image.network(AppConstant.DEFAULT_AVATAR)),
        ),
      ),
      FutureBuilder(
        future: Database.getProfile(postUuid),
        builder: (context, AsyncSnapshot snapshot) =>
        (snapshot.hasData && snapshot.data != null && snapshot.data[AppConstant.NAME]!= null)
            ? Text(snapshot.data[AppConstant.NAME])
            : Text('...'),
      )
    ]);
  }
}
