import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:squadre/Model/UserModel.dart';
import 'package:squadre/screens/ChatPage.dart';
import 'package:squadre/utils/AppConstant.dart' as AppConstants;
import 'package:squadre/utils/Utils.dart';
import 'package:squadre/widgets/ProfileTile.dart';
import 'package:squadre/utils/Firebase.dart' as fbUtils;

class ChatItem extends StatefulWidget {
  DocumentSnapshot data;

  ChatItem(this.data);

  @override
  _ChatItemState createState() => _ChatItemState();
}

class _ChatItemState extends State<ChatItem> {
  String username = '';
  String image = AppConstants.DEFAULT_AVATAR;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(widget?.data.toString());
    return Column(
      children: <Widget>[
        GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ChatPage(
                        widget.data[AppConstants.MESSAGE].toString(),
                        username,
                        image,
                        widget.data.documentID)));
          },
          child: ListTile(
            leading: Padding(
              padding: const EdgeInsets.only(left: 0.0),
              child: CircleAvatar(
                radius: 30.0,
                backgroundColor: Colors.white,
                child: ClipOval(
                  child: FutureBuilder(
                      future: fbUtils
                          .getProfile(widget.data[AppConstants.POST_USER_ID]),
                      builder: (context, snapshot) {
                        if ((snapshot.hasData &&
                            snapshot.data[AppConstants.AVATAR] != null)) {
                          image = snapshot.data[AppConstants.AVATAR];
                        }
                        return Image.network(image);
                      }),
                ),
              ),
            ),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                    child: FutureBuilder(
                  future: fbUtils
                      .getProfile(widget.data[AppConstants.POST_USER_ID]),
                  builder: (context, AsyncSnapshot snapshot) {
                    if ((snapshot.hasData &&
                        snapshot.data[AppConstants.NAME] != null)) {
                      username = snapshot.data[AppConstants.NAME];
                    }
                    return Text(username);
                  },
                )),
                Text(
                  DateFormat.yMMMd().format((widget.data["time"] as Timestamp).toDate()),
                  style: TextStyle(fontSize: 14.0, color: Utils.greyColor),
                )
              ],
            ),
            subtitle: Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Text(
                      widget.data['message'].toString(),
                      style: TextStyle(color: Utils.greyColor),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: Utils.greyColor,
                    size: 15,
                  )
                ],
              ),
            ),
          ),
        ),
        Divider()
      ],
    );
  }
}
