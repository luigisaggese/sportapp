import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:squadre/screens/VideoPreview.dart';
import 'package:squadre/utils/Utils.dart';
import 'package:squadre/utils/AppConstant.dart' as AppConstant;
import 'package:squadre/utils/Firebase.dart' as firebaseService;

import 'ProfileTile.dart';

class PostItem extends StatefulWidget {
  DocumentSnapshot documentSnapshot;
  PostItem({Key key, this.documentSnapshot}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return PostItemState();
  }
}

class PostItemState extends State<PostItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: Utils.deviceWidth,
      margin: EdgeInsets.all(10),
      child: Card(
        elevation: 3,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new ProfileTile(
                  widget.documentSnapshot.data[AppConstant.POST_USER_ID]),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  widget.documentSnapshot.data[AppConstant.MESSAGE] ?? '',
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new FutureBuilder<QuerySnapshot>(
                      future: firebaseService
                          .getReply(widget.documentSnapshot.documentID),
                      //sort: (b, a) => a.value["time"].compareTo(b.value["time"]),
                      builder: (context, snapshot) {
                        if ((snapshot.hasData &&
                            snapshot.data.documents[0].data != null)) {
                          var dataReply = snapshot.data.documents[0].data;
                          return Center(
                              child: Container(
                                  child: GestureDetector(
                            child: Image.asset('assets/images/video_thumb.jpg'),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => VideoPreview(
                                            path: dataReply[
                                                AppConstant.POST_REPLY],
                                          )));
                            },
                          )));
                        }
                        return CircularProgressIndicator();
                      })),
            ],
          ),
        ),
      ),
    );
  }
}
