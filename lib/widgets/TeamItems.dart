import 'package:flutter/material.dart';
import 'package:squadre/utils/AppConstant.dart' as AppConstants;
import 'package:squadre/utils/MyCallback.dart';
import 'package:squadre/utils/Utils.dart';
import 'package:flutter_advanced_networkimage/src/flutter_advanced_networkimage.dart';

class TeamItems extends StatefulWidget {
  OnClickListener callback;
  bool isSelected = false;
  String teamName;

  TeamItems(this.teamName, {this.isSelected = false, this.callback});

  @override
  _TeamItemsState createState() => _TeamItemsState();
}

class _TeamItemsState extends State<TeamItems> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.callback(!widget.isSelected);
      },
      child: Card(
        shape: RoundedRectangleBorder(
            side: BorderSide(
                color: widget.isSelected ? Utils.blueColor : Utils.greyColor,
                width: 3.0),
            borderRadius: BorderRadius.circular(10)),
        margin: EdgeInsets.only(left: 10, right: 10, bottom: 0),
        color: Utils.cardColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/' + widget.teamName + '.png',
              fit: BoxFit.scaleDown,
              height: 100,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Text(
                '${widget.teamName}',
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.w500),
              ),
            )
          ],
        ),
      ),
    );
  }
}
