import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart' as material;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:squadre/Model/UserModel.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:squadre/utils/Translation.dart';
import 'package:squadre/utils/AppConstant.dart' as AppConstant;
import 'package:rxdart/rxdart.dart';

import 'API.dart';
import 'SharedPref.dart';

class Utils {
  static UserModel registerModel = UserModel();
  static API api = API();
  static SharedPref sharedPref = SharedPref();
  static FirebaseMessaging firebaseMessaging = FirebaseMessaging();

  static var refresh = new BehaviorSubject<bool>();

  static material.Color greyColor = material.Color(0xFFababaf);
  static material.Color colorHint = material.Color(0xFFababaf);

  static material.Color textColor = material.Color(0xFF212121);
  static Color transparentColor = Color.fromRGBO(189, 189, 189, 1.0);
  static material.Color colorWhite = material.Color(0xFFffffff);
  static material.Color blackColor = material.Color(0xFF000000);
  static material.Color colorOrange = material.Color(0xFFff5419);
  static material.Color buttonColor = material.Color(0xFFfe7865);
  static material.Color blueColor = material.Color(0xFFf3964eb);
  static material.Color lightBlueColor = material.Color(0xFF4A90E2);
  static material.Color blueShadowColor = material.Color(0x95f3964eb);
  static material.Color cardColor = material.Color(0xFFf9faff);
  static material.Color purpleColor = material.Color(0xFF243b6b);
  static material.Color pinkColor = material.Color(0xFFF86C68);
  static material.Color pinkColor2 = material.Color(0xFFFE5295);
  static Color colorAppBar = Color(0xFFF0F0F0);
  static material.Gradient gradient = LinearGradient(
      colors: [Utils.pinkColor, Utils.pinkColor2],
      begin: const FractionalOffset(0.1, 0.1),
      end: const FractionalOffset(3.0, -0.5),
      stops: [0.0, 0.4],
      tileMode: TileMode.clamp);

  static double deviceHeight = 0.0;
  static double deviceWidth = 0.0;

  static material.MaterialColor primaryColor =
      const material.MaterialColor(0xFF29C0D4, const {
    50: const material.Color(0xFF29C0D4),
    100: const material.Color(0xFF29C0D4),
    200: const material.Color(0xFF29C0D4),
    300: const material.Color(0xFF29C0D4),
    400: const material.Color(0xFF29C0D4),
    500: const material.Color(0xFF29C0D4),
    600: const material.Color(0xFF29C0D4),
    700: const material.Color(0xFF29C0D4),
    800: const material.Color(0xFF29C0D4),
    900: const material.Color(0xFF1D8997)
  });

  static void showMessage(String message) {
    Fluttertoast.showToast(
        msg: message,
        fontSize: 15,
        backgroundColor: blackColor,
        gravity: ToastGravity.BOTTOM,
        textColor: colorWhite,
        toastLength: Toast.LENGTH_LONG);
  }

  static String getTranslation(String text) {
    return localizedValues['en'][text];
  }


  static List<String> getTeamList() {
    var list = new List<String>();
    list.add("atalanta");
    list.add("bologna");
    list.add("cagliari");
    list.add("chievo");
    list.add("empoli");
    list.add("fiorentina");
    list.add("frosinone");
    list.add("genoa");
    list.add("inter");
    list.add("juventus");
    list.add("lazio");
    list.add("milan");
    list.add("napoli");
    list.add("palermo");
    list.add("parma");
    list.add("roma");
    list.add("sampdoria");
    list.add("sassuolo");
    list.add("spal");
    list.add("torino");
    list.add("udinese");
    return list;
  }

  static String readTimestamp(int timestamp) {
    var now = new DateTime.now();
    var format = new DateFormat('h:mm a');
    var date = new DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
    var diff = now.difference(date);
    var time = '';

    if (diff.inSeconds <= 0 ||
        diff.inSeconds > 0 && diff.inMinutes == 0 ||
        diff.inMinutes > 0 && diff.inHours == 0 ||
        diff.inHours > 0 && diff.inDays == 0) {
      time = format.format(date);
    } else if (diff.inDays > 0 && diff.inDays < 7) {
      if (diff.inDays == 1) {
        time = diff.inDays.toString() + ' DAY AGO';
      } else {
        time = diff.inDays.toString() + ' DAYS AGO';
      }
    } else {
      if (diff.inDays == 7) {
        time = (diff.inDays / 7).floor().toString() + ' WEEK AGO';
      } else {
        time = (diff.inDays / 7).floor().toString() + ' WEEKS AGO';
      }
    }

    return time;
  }
//TODO WRITE ONLY CHANGED VALUES
  static void saveUserData() {
    Utils.sharedPref
        .setStringShared(AppConstant.USER_ID, Utils.registerModel.uuid);
    Utils.sharedPref
        .setStringShared(AppConstant.NAME, Utils.registerModel.name);
    Utils.sharedPref
        .setStringShared(AppConstant.SURNAME, Utils.registerModel.surname);

    Utils.sharedPref
        .setBoolShared(AppConstant.IS_ADMIN, Utils.registerModel.isAdmin);

    Utils.sharedPref
        .setStringShared(AppConstant.EMAIL, Utils.registerModel.email);

    Utils.sharedPref.setStringShared(
        AppConstant.FAVORITE_TEAM_NAME, Utils.registerModel.favoriteTeamName);

    Utils.sharedPref.setStringShared(
        AppConstant.AVATAR, Utils.registerModel.avatar);

    Utils.sharedPref
        .setBoolShared(AppConstant.IS_VERIFIED, Utils.registerModel.isVerified);

    Utils.sharedPref
        .setBoolShared(AppConstant.IS_PUSH_ENABLED, Utils.registerModel.isPushEnabled);

    Utils.sharedPref
        .setBoolShared(AppConstant.IS_EMAIL_ENABLED, Utils.registerModel.isEmailEnabled);

    Utils.sharedPref
        .setIntShared(AppConstant.QUESTION_COUNTER, Utils.registerModel.questionCounter);

    Utils.sharedPref
        .setStringShared(AppConstant.QUESTION_TIMEOUT, Utils.registerModel.questionTimeout.toIso8601String());

    FirebaseAnalytics().setUserId(Utils.registerModel.uuid);
    FirebaseAnalytics().setUserProperty(name:AppConstant.QUESTION_COUNTER, value: Utils.registerModel.questionCounter.toString());
    FirebaseAnalytics().setUserProperty(name:AppConstant.QUESTION_TIMEOUT, value: Utils.registerModel.questionTimeout.toIso8601String());
    FirebaseAnalytics().setUserProperty(name:AppConstant.NAME, value: Utils.registerModel.name.toString());
    FirebaseAnalytics().setUserProperty(name:AppConstant.SURNAME, value: Utils.registerModel.surname.toString());
    FirebaseAnalytics().setUserProperty(name:AppConstant.EMAIL, value: Utils.registerModel.email.toString());
    FirebaseAnalytics().setUserProperty(name:AppConstant.FAVORITE_TEAM_NAME, value: Utils.registerModel.favoriteTeamName.toString());
    FirebaseAnalytics().setUserProperty(name:AppConstant.IS_PUSH_ENABLED, value: Utils.registerModel.isPushEnabled.toString());
    FirebaseAnalytics().setUserProperty(name:AppConstant.IS_EMAIL_ENABLED, value: Utils.registerModel.isEmailEnabled.toString());
  }

  static bool validateEmail(String value) {
    bool isMatch = false;
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      isMatch = true;
    }
    return isMatch;
  }

  static Future<void> readUserData() async {
    Utils.registerModel.uuid =
        await Utils.sharedPref.getData(AppConstant.USER_ID);

    Utils.registerModel.avatar =
    await Utils.sharedPref.getData(AppConstant.AVATAR);

    Utils.registerModel.name =
        await Utils.sharedPref.getData(AppConstant.NAME);

    Utils.registerModel.surname =
    await Utils.sharedPref.getData(AppConstant.SURNAME);

    Utils.registerModel.isAdmin =
        await Utils.sharedPref.getData(AppConstant.IS_ADMIN)?? false;

    Utils.registerModel.email =
        await Utils.sharedPref.getData(AppConstant.EMAIL);

    Utils.registerModel.favoriteTeamName =
        await Utils.sharedPref.getData(AppConstant.FAVORITE_TEAM_NAME);

    Utils.registerModel.isVerified =
    await Utils.sharedPref.getData(AppConstant.IS_VERIFIED) ?? false;

    Utils.registerModel.isVerified =
        await Utils.sharedPref.getData(AppConstant.IS_VERIFIED) ?? false;

    Utils.registerModel.isPushEnabled =
        await Utils.sharedPref.getData(AppConstant.IS_PUSH_ENABLED) ?? false;

    Utils.registerModel.isEmailEnabled =
        await Utils.sharedPref.getData(AppConstant.IS_EMAIL_ENABLED) ?? false;

    Utils.registerModel.questionCounter =
        await Utils.sharedPref.getData(AppConstant.QUESTION_COUNTER) ?? 0;

    Utils.registerModel.questionTimeout =  DateTime.now();
    var questionTimeout = await Utils.sharedPref.getData(AppConstant.QUESTION_TIMEOUT);
    if(questionTimeout != null){
      Utils.registerModel.questionTimeout =DateTime.tryParse(questionTimeout);
    }

    print(Utils.registerModel.uuid);
    print(Utils.registerModel.name);
    print(Utils.registerModel.surname);
    print(Utils.registerModel.isAdmin);
    print(Utils.registerModel.isEmailEnabled);
    print(Utils.registerModel.isPushEnabled);
    print(Utils.registerModel.email);
    print(Utils.registerModel.avatar);
    print(Utils.registerModel.favoriteTeamName);
    print(Utils.registerModel.questionCounter);
    print(Utils.registerModel.questionTimeout);

  }
}
