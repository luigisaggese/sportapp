import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:squadre/Model/UserModel.dart';
import 'package:squadre/utils/AppConstant.dart' as AppConstants;
import 'package:squadre/utils/Utils.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../AccountNotVerifiedException.dart';
import 'SharedPref.dart';

Future<String> login(String emails, String passwords) async {
  SharedPref().saveLogin(emails,
      passwords); //saved to send verification link again from "VerificationPending Screen"
  try {
    final firebaseUser = await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: emails, password: passwords);
    Utils.registerModel.isVerified = firebaseUser.isEmailVerified;
    if (firebaseUser.isEmailVerified) {
      Utils.registerModel.uuid = firebaseUser.uid;
      return firebaseUser.uid;
    } else {
      throw AccountNotVerifiedException('Email not verified!');
    }
  } catch (e) {
    throw (e);
  }
}

Future<DocumentSnapshot> getProfile(String uid) async {
  try {
    return Firestore.instance
        .collection(AppConstants.USERS)
        .document(uid)
        .get();
  } catch (e) {
    throw (e);
  }
}

Future<bool> register(
    String email, String pass, String name, String surname) async {
  SharedPref().saveLogin(email,
      pass); //saved to send verification link again from "VerificationPending Screen" //TODO

  try {
    final firebaseAuth = await FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: pass);
    firebaseAuth.sendEmailVerification();

    Utils.registerModel.name = name;
    Utils.registerModel.email = email;
    Utils.registerModel.surname = surname;
    Utils.registerModel.uuid = firebaseAuth.uid;

    userDetails(Utils.registerModel);

    return true;
  } catch (e) {
    throw (e);
  }
}

Future<bool> forgotPassword(String email) async {
  try {
    await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
    return true;
  } catch (e) {
    throw (e);
  }
}

void userDetails(UserModel registerModel) {
  final collectionReference = Firestore.instance.collection(AppConstants.USERS);

  collectionReference.document(registerModel.uuid).setData({
    AppConstants.NAME: registerModel.name,
    AppConstants.EMAIL: registerModel.email,
    AppConstants.IS_ADMIN: Utils.registerModel.isAdmin,
    AppConstants.IS_PUSH_ENABLED: Utils.registerModel.isPushEnabled,
    AppConstants.IS_EMAIL_ENABLED: Utils.registerModel.isEmailEnabled,
    AppConstants.SURNAME: registerModel.surname,
    AppConstants.AVATAR: registerModel.avatar,
  }, merge: true).then((_) {
    Utils.registerModel.name = registerModel.name;
    Utils.registerModel.email = registerModel.email;
    Utils.registerModel.surname = registerModel.surname;
    Utils.registerModel.uuid = registerModel.uuid;
    Utils.registerModel.isAdmin = registerModel.isAdmin;
    Utils.registerModel.isPushEnabled = registerModel.isPushEnabled;
    Utils.registerModel.isEmailEnabled = registerModel.isEmailEnabled;
    Utils.registerModel.avatar = registerModel.avatar;
    Utils.saveUserData();
  });
}

Future<DocumentReference> addPost(String message, String teamName) async {
  try {
    return Firestore.instance
        .collection(AppConstants.POSTS)
        .document()
        .setData({
      AppConstants.TIME: FieldValue.serverTimestamp(),
      AppConstants.TEAM_NAME: teamName,
      AppConstants.MESSAGE: message,
      AppConstants.POST_USER_ID: Utils.registerModel.uuid
    }).then((_) {
      Firestore.instance
          .collection(AppConstants.USERS)
          .document(Utils.registerModel.uuid)
          .setData({
        AppConstants.QUESTION_COUNTER: FieldValue.increment(1),
        AppConstants.QUESTION_TIMEOUT:
            DateTime.now().add(new Duration(hours: 1)),
      }, merge: true);
      Utils.registerModel.questionCounter =
          Utils.registerModel.questionCounter + 1;
      Utils.registerModel.questionTimeout =
          DateTime.now().add(new Duration(hours: 1));

      Utils.saveUserData();
    });
  } catch (e) {
    throw (e);
  }
}

Future<bool> addPostReply(
    String reply, String userId, String postId, bool isVideo) async {
  try {
    Firestore.instance
        .collection(AppConstants.POSTS + '/' + postId + "/replies")
        .document()
        .setData({
      AppConstants.TIME: FieldValue.serverTimestamp(),
      AppConstants.POST_REPLY: reply,
      AppConstants.POST_USER_ID: userId,
      AppConstants.POST_IS_VIDEO: isVideo
    }, merge: true).then((_) {
      Firestore.instance
          .collection(AppConstants.POSTS)
          .document(postId)
          .setData({AppConstants.POST_IS_REPLY: true}, merge: true);
    });
    return true;
  } catch (e) {
    throw (e);
  }
}

Future<bool> setUserNotificationAndProfile(
    String url, bool isPushEnabled, bool isEmailEnabled) async {
  getProfile(Utils.registerModel.uuid).then((documentReference) {
    if (documentReference.exists) {
      var finalMap = Map<String, dynamic>.from(documentReference.data);

      if (url != null) {
        finalMap[AppConstants.AVATAR] = url;
        Utils.registerModel.avatar = url;
      }
      finalMap[AppConstants.IS_PUSH_ENABLED] = isPushEnabled;
      finalMap[AppConstants.IS_EMAIL_ENABLED] = isEmailEnabled;

      print("data is " + finalMap.toString());
      //TODO Check if is possible to update document reference
      if (finalMap.length != 0) {
        Firestore.instance
            .collection(AppConstants.USERS)
            .document(documentReference.documentID)
            .setData(finalMap, merge: true)
            .then((_) {
          Utils.registerModel.isPushEnabled = isPushEnabled;
          Utils.registerModel.isEmailEnabled = isEmailEnabled;
          Utils.saveUserData();
          return true;
        });
      }
    }
    return false;
  });
}

Future<bool> setTeam(String name) async {
  getProfile(Utils.registerModel.uuid).then((documentReference) {
    if (documentReference.exists) {
      var finalMap = Map<String, dynamic>.from(documentReference.data);

      if (name.isNotEmpty) {
        finalMap[AppConstants.FAVORITE_TEAM_NAME] = name;
      }

      print("data is " + finalMap.toString());
      //TODO Check if is possible to update document reference
      if (finalMap.length != 0) {
        Firestore.instance
            .collection(AppConstants.USERS)
            .document(documentReference.documentID)
            .setData(finalMap, merge: true)
            .then((_) {
          Utils.registerModel.favoriteTeamName = name;
          Utils.saveUserData();
          return true;
        });
      }
    }
    return false;
  });
}

Future<bool> setToken(String token) async {
  getProfile(Utils.registerModel.uuid).then((documentReference) {
    if (documentReference.exists) {
      var finalMap = Map<String, dynamic>.from(documentReference.data);

      if (token.isNotEmpty) {
        finalMap[AppConstants.TOKEN] =
            token; //TODO DO A COLLECTION FOR MULTIPLE DEVICE
      }

      print("data is " + finalMap.toString());
      //TODO Check if is possible to update document reference
      if (finalMap.length != 0) {
        Firestore.instance
            .collection(AppConstants.USERS)
            .document(documentReference.documentID)
            .setData(finalMap, merge: true)
            .then((_) {
          return true;
        });
      }
    }
    return false;
  });
}

Future<QuerySnapshot> getReply(String postUUId) {
  try {
    return Firestore.instance
        .collection(AppConstants.POSTS + '/' + postUUId + '/replies').getDocuments();
  } catch (e) {
    throw (e);
  }
}

Stream<QuerySnapshot> getPosts() {
  try {
    return Firestore.instance.collection(AppConstants.POSTS).snapshots();
  } catch (e) {
    throw (e);
  }
}

Future<bool> resendVerificationLink() async {
  String email = await SharedPref().readStringValue(AppConstants.EMAIL);
  String password = await SharedPref().readStringValue(AppConstants.PASSWORD);

  try {
    final firebaseUser = await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password);
    firebaseUser.sendEmailVerification();
    return true;
  } catch (e) {
    throw (e);
  }
}
