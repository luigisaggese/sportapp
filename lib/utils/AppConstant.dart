import 'package:flutter/material.dart';

//Firebase keys

//Parents
const String USERS = "users";
const String POSTS = "posts";
const String REPLIES = "replies";
//register
const String NAME = "name";
const String USER_ID = "uuid";
const String EMAIL = "email";
const String PASSWORD = "password";
const String SURNAME = "surname";
const String IS_ADMIN = "isAdmin";
const String FAVORITE_TEAM_NAME = "favoriteTeamName";
const String IS_VERIFIED = "isVerified";
const String QUESTION_COUNTER = "questionCounter";
const String QUESTION_TIMEOUT = "questionTimeout";

//add posts
const String POST_ID = "postId";
const String TIME = "time";
const String TEAM_NAME = "teamName";
const String MESSAGE = "message";
const String POST_USER_ID = "postUserId";
const String POST_IS_VIDEO = "isVideo";
const String POST_REPLY = "reply";
const String POST_IS_REPLY = "isReplied";
const String POST_REPLY_TIME = "time";
const String POST_NUMBER = 'POST_NUMBER';
const String TOKEN = 'TOKEN';

String apiToken;
//API Key

const String AVATAR = "avatar";
const String URL = "url";

//Translation map keys

const String USERNAME_ = "username";
const String EMAIL_ = "email";
const String DEFAULT_AVATAR =
    "https://www.ekahiornish.com/wp-content/uploads/2018/07/default-avatar-profile-icon-vector-18942381.jpg";
const String IS_PUSH_ENABLED = "isPushEnabled";
const String IS_EMAIL_ENABLED = "isEmailEnabled";