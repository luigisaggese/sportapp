'use strict';
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { DataSnapshot } from 'firebase-functions/lib/providers/database';
import * as nodemailer from 'nodemailer';

const mailTransport = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  requireTLS: true,
  auth: {
    user: 'luigisag@gmail.com',
  },
});


admin.initializeApp();

const db = admin.database()

const subjectNotification = 'Hai ricevuto una risposta alla tua domanda';
exports.sendMessegeNotification = functions.database.ref('/posts/{postUuid}/replies')
  .onCreate((messageSnapshot: DataSnapshot, context: functions.EventContext) => {

    console.log('context.params', context.params);

    return messageSnapshot.ref.parent!.child('postUserId').once('value').then(postUserIdSnapshot => {

      console.log('postUserIdSnapshot', postUserIdSnapshot);
      const postUserId = postUserIdSnapshot.val();
      console.log('postUserId', postUserId);

      return messageSnapshot.ref.parent!.child('message').once('value').then(questionSnapshot => {
        const question = questionSnapshot.val();
        console.log('question', question);

        //query DB
        return db.ref('users/' + postUserId + '/token').once('value', async (userInfoSnapshot: any) => {
          const registrationToken = userInfoSnapshot.val();
          // check user details in firebase console
          console.log('registrationToken', registrationToken);

          userInfoSnapshot.ref.parent.child('email').once('value').then((emailSnapshot : DataSnapshot)=> {
            const email = emailSnapshot.val();
            console.log('email', email);

            const mailOptions = {
              from: '"Soccer App." <noreply@soccerapp.com>',
              to: email,
              subject: subjectNotification,
              text: question 
            };
            try{
              console.log('mailOptions', mailOptions);

              mailTransport.sendMail(mailOptions, (error, info) => {
                if (error) {
                  console.log('Error sending email to user', error);
                }
                console.log('Succesfull sent', info);
              });

            }
            catch(error){
              console.error('Error sending email', error);
            }
          });


          //Create message object for notification
          const message = {
            notification: {
              title: subjectNotification,
              body: question
            },
            data: {
              postUuid: context.params.postUuid,
            }
          };
          console.log('message', message);

          // registration token.
          return admin.messaging().sendToDevice(registrationToken, message)
            .then(function (response) {
              // Response is a message ID string.
              console.log('Successfully sent message:', response);
            })
            .catch(function (error) {
              console.log('Error sending message:', error);
            });
        });
      });
    });
  });
