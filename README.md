# SportApp

The app it is a sort of chat for question and answer. There are two types of users: the one who asks the questions and the one who answers.
The workflow is as follows. A user downloads the app, opens it and can ask a question on a sport team every Y hours (DB configured). 

The question is in text format (with filter on the type of content anti sexual texting or spam). The question administrator login and see all questions not answered categorized by team. So there are N people who can ask questions (where N is the number of devices that have downloaded the app) and M predefined adminstrators who answer (isAdmin = true into DB).

The administrator who answers can reply with a video (or mark question as spam) and the answer will be show in a feed to all users (the feed contain all questions of last 24 hours). Login/signup with FB/Instagram is required (this one is also used to distinguish between the people that do the question and the only one that reply). There is no web portal, the person that will answer will do within the app. App include notifications on different events (received reply, question, expiring time). The app need is developed using Flutter as frontend and Firebase chat as backend.

Into this document all the features, part of them are already implemented
https://docs.google.com/document/d/1nYEYs8xdWC1h2mvDjjiHsbp7NtAB6F7OD3Nh2DFx428/edit?usp=sharing

Question & Answer app 

![image alt text](images/image_0.png)

The app it is a sort of chat for question and answer. There are two types of users: the one who asks the questions and the one who answers.

The workflow is as follows. A user downloads the app, opens it and can ask a question on a sport team every 24 hours. The question is in text format (with filter on the type of content anti sexual texting or spam). The question administrator login and see all questions not answered categorized by team. So there are N people who can ask questions (where N is the number of devices that have downloaded the app) and M predefined adminstrators who answer. The administrator who answers can reply with a video (or mark question as spam) and the answer will be show in a feed to all users (the feed contain all questions of last 24 hours). Login/signup with FB/Instagram is required (this one is also used to distinguish between the people that do the question and the only one that reply). There is no web portal, the person that will answer will do within the app. App include notifications on different events (received reply, question, expiring time). The app need to be developed using Flutter as frontend and Firebase chat as backend, but I’m open to listen other tech idea.

Complete understanding of the mobile application

Platform: Flutter

**Login Screen**

![image alt text](images/image_1.png)

* **Log in with Email & Password - **Once user will successfully be authenticated then it will redirect on the particular page: 
** i)** If user first-time login in the app after register, It will redirect on the "Landing Page" -> Team selection Page, Where user will select a favorite team. 

 **ii) ** If a user already selected the team as a favorite then it will redirect on feed screen.

* Login with Facebook, Google, Twitter & Instagram 

* **Forgot password option** - It will redirect on forgot password screen

* **Signup** - It will redirect on register screen.

**Login with Facebook, Google, Twitter  & Instagram** - User profile data fetched based on the user privacy settings, Sometimes user applies privacy on social media account for his/her own information. 

Possible information you will fetch from the user social media account based on the user privacy data and create app account according to that.

**Forgot password Screen**

* A user will fill register email id for resetting the password in the text field.

* An email will be a sent on register email with instruction, Where the user can reset the password.

* Password reset email will work only if the user registered with the email address instead of a social media account.  

**Register Screen **

* **Full Name** - Text field where a user will enter his/her full name. 

* **Profile picture **- User can pick an image from Camera or gallery.

* **Email** - Text field where the user will fill a valid email address. (if the user already registered with entered email id then we will show an error message.)

* **Location - **It will be text field user can fill address.

* **Password** - Textfield where the user will enter a password (Password should be minimum 6 digits)

* The user can enter his/her location or address.

* **Confirm Password** - Text field where the user will enter the same password which entered in the password field. 

**Landing Screen**

![image alt text](images/image_2.png)

* When the user will successfully be logged in the app first time then this screen appears for the user which will redirect on the team selection screen. 

**SELEZIONA SQUADRA (Team selection Screen) **

![image alt text](images/image_3.png)

* It will display a list of a team where a user can select any of the team as a favorite.

* Once the user will select the team and press on check mark then it will redirect on the question asking screen, Where the user can ask a question.

**Question Asking Screen**

![image alt text](images/image_4.png)

* The user can redirect on screen from feed or team selection screen.

* The user can ask a question in text format. 

* After entering the question a user will tap on the check mark then after question will save in firebase where other side admin can view and answer the question. 

* Default selection favorite team, but can ask question on any team

**Feed Screen**

![image alt text](images/image_5.png)

* The user can view all the feed (Question and Answer of all teams) with time duration (Visibility time for the particular item in feed) and video lenght

* Only those questions will appear in the feed which answered provided by the admin.

* Each feed item will be available for X hours only then it’s gets removed automatically from the feed (X configured in firebase)

* The user can select a his/her favorite team to filter feed. 

* The user can redirect on the question asking screen and user profile screen. 

**Profile Screen**

![image alt text](images/image_6.png)

* The screen will show a user profile image with name and location (If user location exists for the user).

* Show number of questions asked by the user.

* A number of hours left for asking the next question. 

* Show user favorite team icon with the name.

* The user can redirect on the feed screen.

* The user can also redirect on the profile edit screen for a profile update.

* Once the user will tap on the number of questions  button then it will redirect on the reply screen, Where a user can see asked questions and it answers. (ONLY READ MODE)

**Profile Edit Screen - **

![image alt text](images/image_7.png)

* The user can edit the profile image and password as well.

* The user can change also the favorite team.

* The user can enable/disable "Notification via email" and “Push Notification”.

* There is showing only one field for password change but generally password change contains "Current Password", “New Password” and “Confirm Password” (to updarte UI)

 

 

**Risposte (Reply Screen)**

![image alt text](images/image_8.png)

* The user can view all the asked questions and it answers.

* This screen will be read-only for the user. 

* This Screen will be read and write only for the admin, Where admin can answer the question by "Text", “Image” or “Video”. 

* Admin can answer the question only with any of the format (Text, Image or Video).

* Admin can go back onto the previous screen through the back button (top left side).

* Admin can view user profile if tap on the user profile icon.

* All the questions and answers will appear with a timestamp.

**Admin Screen -** (In the app only email admins (defined in firebase) will be available which received all the questions and answer according to that. It will define at the starting with his/her **"Email" & “Password”** or Social login

![image alt text](images/image_9.png)

* This screen will show all the team icons with a number of question in the badge on top of the screen.

* Once admin tap on the particular team icon then all the users list will appear which asked a question for the particular team and waiting for the answer.

* Admin can search the user through search field. 

* Once admin tap on the particular user then he will redirect on the reply screen where they can answer for the question. 

